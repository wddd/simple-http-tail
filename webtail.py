# -*- coding: utf-8 -*-

"""
HTTP server that provides a web interface to run "tail" on log files,
like the Unix command.
This is a standalone script. No external dependencies required.
How to invoke:
    python webtail.py [port]
Where:
    - port is the port number where the webtail server will listen.
"""

import collections
import fnmatch
import os
from cgi import escape
from urlparse import parse_qs
from wsgiref.simple_server import make_server

# https://stackoverflow.com/a/5466478
LIST_HTML = """
<html>
<head>
<title>Web Tail</title>
<style>
body {
    background: black; 
    color: #ddd;
}
a {
    color: #fff;
    opacity: 0.8;
}
</style>
</head>
<body>
%s
</body>
</html>
"""

TAIL_HTML = """<html>
<head>
<title>Web Tail</title>
<script type="text/javascript">
var offset = 0;
var polling = null;
var param = function (key, fallback) {
    var query = window.location.search.substring(1);
    var parameters = query.split('&');
    for (var i = 0; i < parameters.length; i++) {
        var pair = parameters[i].split('=');
        if (pair[0] == key) {
            return unescape(pair[1]);
        }
    }
    return fallback;
}
var append = function (text) {
    if (text) {
        var element = document.getElementById('tail');
        element.textContent += text;
        window.scrollTo(0, document.body.scrollHeight);
    }
}
var request = function (uri, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', uri, true);
    xhr.onreadystatechange = function () {
        var done = 4, ok = 200;
        if (xhr.readyState == done && xhr.status == ok) {
            var newOffset = xhr.getResponseHeader('X-Seek-Offset');
            if (newOffset) offset = parseInt(newOffset);
            callback(xhr.responseText);
        }
    };
    xhr.send(null);
}
var tail = function () {
    var uri = '/get_tail?file_path=' + param('file_path') + '&offset=' + offset;
    if (!offset) {
        var limit = parseInt(param('limit', 1000));
        uri += '&limit=' + limit;
    }
    request(uri, append);
}
var refresh = function () {
    tail();
    if (polling == null) {
        var interval = parseInt(param('interval', 3000));
        polling = window.setInterval(tail, interval);
    }
}
var sleep = function () {
    if (polling != null) {
        window.clearInterval(polling);
        polling = null;
    }
}
document.title = param('file_path', 'Web Tail');
window.onload = refresh;
window.onfocus = refresh;
window.onblur = sleep;
</script>
</head>
<body style="background: black; color: #ddd;">
<pre id="tail"></pre>
</body>
</html>
"""


# https://stackoverflow.com/a/2186565
def list_files():
    files = []
    for root, _, filenames in os.walk('../'):
        for filename in fnmatch.filter(filenames, '*.log'):
            files.append(os.path.join(root, filename))

    return LIST_HTML % list_to_html_links(files)


def list_to_html_links(files):
    link_text = '<a href="{link}">{text}</a>'

    def generate(file_path):
        link = '/tail?file_path={file_path}&offset=0&limit=100'.format(file_path=file_path)
        return link_text.format(link=link, text=file_path)

    return '<br/> '.join([generate(file_path_str) for file_path_str in files])


def get_tail(environ, response_headers):
    query = parse_qs(environ['QUERY_STRING'])
    file_path = get_query_string(query, 'file_path')

    if not file_path.endswith('.log'):
        return None

    offset = int(get_query_string(query, 'offset', '0'))
    limit = int(get_query_string(query, 'limit', '0')) or None

    return _get_tail(file_path, offset, limit, response_headers)


def get_query_string(query, query_argument, default=''):
    result = query.get(query_argument, [default])[0]
    return escape(result)


def _get_tail(file_path, offset, limit, response_headers):
    response_headers['Content-Type'] = 'text/plain; charset=UTF-8'
    size = os.stat(file_path).st_size
    if size <= offset:
        # logging.info('tail returned empty string with stat optimization')
        response_headers['X-Seek-Offset'] = str(size)
        return ''
    new_offset, lines = tail(file_path, offset, limit)
    # logging.info('tail(%r, %r, %r) returned offset %d and %d lines', \
    #              self.filename, offset, limit, new_offset, len(lines))
    response_headers['X-Seek-Offset'] = str(new_offset)
    return ''.join(lines)


def tail(file_path, offset=0, limit=None):
    """ Returns lines in a file (from given offset, up to limit lines) """
    lines = collections.deque([], limit)
    stream = open(file_path)
    stream.seek(offset)
    for line in stream:
        if not line.endswith('\n'):  # ignore last line if incomplete
            break
        lines.append(line)
        offset += len(line)
    stream.close()
    return offset, lines


def webtail(environ, start_response):
    path_info = environ['PATH_INFO']
    response_headers = {}

    routes = {
        '/': lambda: list_files(),
        '/tail': lambda: TAIL_HTML,
        '/get_tail': lambda: get_tail(environ, response_headers)
    }

    response_body = routes.get(path_info, lambda: None)()

    if response_body is None:
        response_body = ''
        status = '400 Not Found'
    else:
        status = '200 OK'

    if 'Content-Type' not in response_headers:
        response_headers['Content-Type'] = 'text/html; charset=UTF-8'
    response_headers['Content-Length'] = str(len(response_body))

    start_response(status, response_headers.items())

    return [response_body]


httpd = make_server('0.0.0.0', 1987, webtail)
print 'Serving HTTP on port 1987...'
httpd.serve_forever()
