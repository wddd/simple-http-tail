# -*- coding: utf-8 -*-

"""
Build-in demo.
https://docs.python.org/2/library/wsgiref.html#wsgiref.simple_server.demo_app
"""

from wsgiref.simple_server import make_server, demo_app

# Instantiate the server
httpd = make_server(
    '0.0.0.0',  # The host name
    1986,  # A port number where to wait for the request
    demo_app  # The application object name, in this case a function
)
print 'Serving HTTP on port 1986...'

# Respond to requests until process is killed
# httpd.serve_forever()

# Alternative: wait for a single request, serve it and quit
httpd.handle_request()
