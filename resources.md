Resources
======

Web Server and web application:

* A web application consists of application code written to perform some action(s) on a web server and display the result to a web browser client.
* A web server is the software operating on a computer, to which web browsers connect using HTTP/HTTPS to run web applications in the background, and see the results. 
* https://stackoverflow.com/a/936352
* https://www.quora.com/What-is-the-difference-between-web-server-and-web-application

Python Web server and web application resources:

* https://ruslanspivak.com/lsbaws-part1/
* SimpleHTTPServer: https://docs.python.org/2/library/simplehttpserver.html
* BaseHTTPServer: https://docs.python.org/2/library/basehttpserver.html
* WSGI:
  * https://stackoverflow.com/a/33244760
  * https://docs.python.org/2/library/wsgiref.html
  * https://wsgi.readthedocs.io/en/latest/learn.html
  * http://wsgi.tutorial.codepoint.net/intro
* Webtail: https://gist.github.com/scoffey/1617014
